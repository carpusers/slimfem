
#include <stdio.h>
#include <iostream>
#include <string>
#include <mpi.h>

#include "src/SF_base.h"


int main(int argc, char** argv)
{
  MPI_Init(&argc, &argv);
  MPI_Comm comm = SF_COMM;

  int rank, size;
  MPI_Comm_size(comm, &size); MPI_Comm_rank(comm, &rank);

  SF::vector<int> indices, counts;
  SF::vector<char> values;

  switch(rank) {
    default:
    case 0:
      indices.resize(3), counts.resize(3);
      indices[0] = 6, indices[1] = 2, indices[2] = 4;
      counts[0] = 4, counts[1] = 2, counts[2] = 3;
      break;

    case 1:
      indices.resize(2), counts.resize(2);
      indices[0] = 0, indices[1] = 3;
      counts[0] = 2, counts[1] = 2;
      break;

    case 2:
      indices.resize(4), counts.resize(4);
      indices[0] = 1, indices[1] = 8, indices[2] = 7, indices[3] = 5;
      counts[0] = 1, counts[1] = 3, counts[2] = 3, counts[3] = 6;
      break;

  }
  values.resize(SF::sum(counts));
  for(size_t i=0, idx=0; i<counts.size(); i++)
    for(size_t j=0; j<counts[i]; j++, idx++)
      values[idx] = rank + i;

  SF::vector<int> srt_idx, srt_cnt;
  SF::vector<char> srt_val;

  for(int pid = 0; pid < size; pid++) {
    if(rank == pid) {
      for(size_t i=0, ridx=0; i<counts.size(); i++)
        for(size_t j=0; j<counts[i]; j++, ridx++)
          printf("Rank %d: idx = %d, cnt = %d, val = %d\n",
                 rank, indices[i], counts[i], values[ridx]);
      fflush(stdout);
    }
    MPI_Barrier(comm);
  }

  sleep(2);
  MPI_Barrier(comm);


  if(rank == 0)
    printf("\n\n==================================\n\n");

  SF::sort_parallel(comm, indices, counts, values, srt_idx, srt_cnt, srt_val);

  for(int pid = 0; pid < size; pid++) {
    if(rank == pid) {
      for(size_t i=0, idx=0; i<srt_cnt.size(); i++)
        for(size_t j=0; j<srt_cnt[i]; j++, idx++)
          printf("Rank %d: idx = %d, cnt = %d, val = %d\n",
                 rank, srt_idx[i], srt_cnt[i], srt_val[idx]);
    }
    MPI_Barrier(comm);
  }


  sleep(2);

  MPI_Barrier(comm);
  if(rank == 0)
    printf("\n\n======== Simple sort test ==================\n\n");

  values.assign(indices.size(), rank);

  for(int pid = 0; pid < size; pid++) {
    if(rank == pid) {
      for(size_t i=0; i<indices.size(); i++)
        printf("Rank %d: idx = %d, val = %d\n", rank, indices[i], values[i]);
    }
    MPI_Barrier(comm);
  }

  sleep(2);

  MPI_Barrier(comm);
  if(rank == 0)
    printf("\n\n==================================\n\n");

  SF::sort_parallel(comm, indices, values, srt_idx, srt_val);

  for(int pid = 0; pid < size; pid++) {
    if(rank == pid) {
      for(size_t i=0; i<srt_idx.size(); i++)
        printf("Rank %d: idx = %d, val = %d\n", rank, srt_idx[i], srt_val[i]);
    }
    MPI_Barrier(comm);
  }

  MPI_Finalize();
  return 0;
}


