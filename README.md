# SlimFem - A Lightweight Header-Only Mesh-Management and FEM Framework
Author: Aurel Neic

## Introduction

A main part of the framework deals with the problem of managing multiple, partly overlapping
submeshes which are all extract from one reference mesh.

### Motivation

While the simulation domain can always be described in one mesh - the _reference_ mesh - 
there can be many reasons that require to derive a submesh (i.e. a subset of the element
set of the reference mesh) from the reference mesh.

In the context of multi-physics simulations, the most common reason is that one wants to
restrict the domain of each simulated physics to a domain where the functions of interest
are meaningfully represented and the domain itself is of minimal size. 
For example, in a electro-mechano-fluidic simulation of the left ventricle of a heart,
the following submeshes might need to be derived:

* Cardiac electrics are represented on two meshes. One, consisting of the myocardial volume,
  denotes the intracellular domain and the other, consisting of the myocardium, the blood pool
  and some volume outside the myocard, the extracellular one.
* Cardiac mechanics typically is simulated on a mesh similar to the intracellular electrics mesh.
* Hemodynamics is simulated on a discretization of the blood pool.

### Challenges

Once a submesh has been defined from the reference mesh, the entities of the mesh
(i.e. the element and vertices) are renumbered and repartitioned.
As a consequence, meshes derived from the reference mesh have multiple numberings
(at least two - that of the reference mesh and the one of the submesh - but possibly more).
Keeping track of the different numberings and providing a mapping functionality that is
accessible to the user, consistent and also performant in a parallel environment, is a key
feature of a mesh management code.

## Mesh-Management Concepts

### Tag based subdomain definitions

Subdomains are only defined through sets of tags.
Using multiple mechanisms for identifying domain regions introduces errors and weakens the
meaning of each individual mechanism.
  
### Element redistribution

Element data is communicated via one general redistribution function, the element redistribution.
Based on any kind of initial distribution, the function allows to assign a destination process
to each element and then exchange the elements.
The generality of this approach allows gather/scatter - like operations as well as
domain-decomposition based redistributions.

### Elements before vertices

The element data of all (sub)meshes is set up before the vertex coordinates are read.
This has the advantage that the coordinates are no hindrance during mesh redistribution
and need to be communicated only once for all submeshes.
Still, vertex coordinates can also be redistributed at a later stage.

### Local indexing only

The element (connectivity) definitions are stored only in local indexing.
The different global numberings can be retrieved via the associated numbering data-structures.

### Multiple numberings

Arbitrary many global numberings can be associated to a mesh. Accessing the different global
indices of one local index is a trivial array access.

## Building

SlimFem is a header-only library. Therefore, its functionality is provided by including the `SF_base.h`
header file into your project.

## Test Binary

A test binary, showcasing the main mesh-mangement functionality of SlimFem, can be build via

    make test

The parallel test is then executed via

    mpirun -np N ./test CARP-MESH

## Documentation

The documentation can be built via

    make doc-doxy

The html doxygen documentation is then located in `doc/html`. It can be viewed via

    firefox doc/html/index.html

As a new user, you probably want to browse the _files_ tab to get an idea about the functionality provided
by each SlimFem.


